#!/usr/bin/env python
"""S2 database"""

import json
import urllib.request

import datetime as dt

from functools import partial
from concurrent.futures import ThreadPoolExecutor, as_completed
from collections import defaultdict


from tqdm.autonotebook import tqdm

import numpy as np
from osgeo import gdal



def get_url(url, poly):
    kwds = {"cutlineDSName": poly, "cropToCutline": True,
            "dstNodata":3500}
    g1 = gdal.Warp("", url, format="MEM", **kwds)
    data = g1.ReadAsArray()
    datax = data / 100.
    datax[data==3500] = np.nan
    return datax


class S2LAIdb(object):
    def __init__(self, url="http://www2.geog.ucl.ac.uk/~ucfajlg/uk_tester/s2_db.json"):
        self.db = self._get_db(url)

    def _get_db(self, url):
        r = urllib.request.urlopen(url).read().decode("utf-8")
        db_temp = json.loads(r)
        acq_times = sorted([k for k in db_temp.keys()])
        db = {}
        for k in acq_times:
            t = dt.datetime.strptime(k, "%Y%m%d")
            db[t] = db_temp[k]
        return db


    def get_time_series(self, poly):
        analysis_data = defaultdict(dict)

        grabber = partial(get_url, poly=poly)
        with ThreadPoolExecutor(5) as executor:
            futures = []
            for date, url in self.db.items():
                futures.append(
                    executor.submit(grabber, url)
                )            
            
        data = [x.result() for x in as_completed(futures)]
        keys = [k for k in self.db.keys()]
        analysis_data = dict(zip(keys, data))
        
        return analysis_data
