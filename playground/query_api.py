#!/usr/bin/env python
"""Getting field boundaries like a boss"""

import requests
import json
import os

from urllib.parse import quote

import numpy as np





def get_wkt_polygon(feature_collection):
    wkt = "POLYGON((" + ",".join([
        f"{x[0]} {x[1]}" for x in 
            feature_collection['features'][0]['geometry']['coordinates'][0]]) \
            + "))"
    return wkt


def calc_centroid(feature_collection):
    x = [x[0] for x in feature_collection['features'][0]['geometry']['coordinates'][0]]
    y = [x[1] for x in feature_collection['features'][0]['geometry']['coordinates'][0]]  
    centroid = np.mean([x,y], axis=1)
    return centroid[::-1]

def get_field_boundary(field_id, api_key):
    BASE_URL = "https://api.agrimetrics.co.uk/field-boundaries"
    url = f"{BASE_URL}/{field_id}?subscription-key={api_key}"

    geojson = True
    headers = {}
    if geojson:
        headers["accept"] = "application/geo+json"

    response = requests.get(url=url, headers=headers)
    return response.json()
    

def get_all_field_boundaries(polygon, api_key):
    # Build up the URL    
    BASE_URL = "https://api.agrimetrics.co.uk/field-boundaries"
    url = f"{BASE_URL}?geometry={quote(polygon)}&op=within&subscription-key={api_key}"
    # Can potentially get lots of results, so annoying paging...
    headers={}
    # Page per hundred fields
    page_number = 1
    page_size = 100
    features = []
    while True:
        the_url = f"{url}&pageSize={page_size}&pageNum={page_number}"
        response = requests.get(url=the_url, headers=headers)
        retval = response.json()
        features += retval['results']
        if len(retval) < page_size:
            break
    
    # Create output GJ string
    
    zz = {'type':"FeatureCollection",
          'features':features}
    return zz

    

    
def find_field_texture(lat, lon, api_key):
    url = f"https://api.agrimetrics.co.uk/field-finder?lat={lat}&lon={lon}&subscription-key={api_key}"
    req = requests.get(url)
    response = req.json()
    field_name = response['@id'].split("/")[-1]
    soil_textures = ["clay", "sand", "silt"]
    field_texture = {}
    for api in ["field-facts"]:# you could add more apis "field-trends"]:
        url = f"{response['_links'][f'ag:api:{api}']['href']}?subscription-key={api_key}"
        req = requests.get(url)
        rex = req.json()
        if api == "field-facts":
            field_texture = {texture: 
                            rex['hasSoilLayer'][0]["hasSoilTexture"][f'{texture}Percentage']
                            for texture in soil_textures}
    return field_name, field_texture



    
